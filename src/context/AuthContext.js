// AuthContext.js
import React, { useState, useEffect, createContext, useContext } from "react";
import { auth } from "../firebaseConfig/firebase";
import {
  createUserWithEmailAndPassword,
  signInWithEmailAndPassword,
  signOut,
  onAuthStateChanged,
} from "firebase/auth";
import { getFirestore, collection, addDoc } from "firebase/firestore";

export const authContext = createContext();

export const useAuth = () => {
  const context = useContext(authContext);
  if (!context) {
    console.error("Error: No se creó el contexto");
  }
  return context;
};

export const AuthProvider = ({ children }) => {
  const [user, setUser] = useState(null);

  const checkAuthentication = () => {
    return new Promise((resolve, reject) => {
      const unsubscribe = onAuthStateChanged(auth, (user) => {
        unsubscribe();
        if (user) {
          resolve(user);
        } else {
          reject(new Error("Usuario no autenticado"));
        }
      });
    });
  };

  const onAuthStateChangedHandler = (callback) => {
    return onAuthStateChanged(auth, (currentUser) => {
      if (!currentUser) {
        //console.log("Usuario no logueado");
        setUser(null);
      } else {
        setUser(currentUser);
        //console.log("logueado;", currentUser);
      }
      if (typeof callback === "function") {
        callback(currentUser);
      }
    });
  };

  useEffect(() => {
    const suscribed = onAuthStateChangedHandler();
    return () => suscribed();
  }, []);

  const register = async (email, password, name) => {
    try {
      // Crear usuario sin autenticación automática
      const response = await createUserWithEmailAndPassword(auth, email, password);
      const user = response.user;
  
      if (name) {
        const db = getFirestore();
        await addDoc(collection(db, "users"), {
          uid: user.uid,
          name: name,
          email: user.email
        });
      }
  
      //console.log(response);

      setUser(null);
  
      return 'Registro exitoso';
  
    } catch (error) {
     //console.error("Error en register:", error);
  
      if (error.message) {
        //console.error("Mensaje de error:", error.message);
  
        if (error.message.includes('auth/email-already-in-use')) {
          throw new Error('El correo electrónico ya está en uso. Intente con otro correo.');
        } else if (error.message.includes('auth/weak-password')){
          throw new Error('La contraseña debe tener 6 caracteres');
        }else{
          throw new Error('Ha ocurrido un error durante el registro. Por favor, inténtelo de nuevo.');
        }
      }
    }
  };
  
  

  const login = async (email, password) => {
    try {
      const response = await signInWithEmailAndPassword(auth, email, password);
      //console.log(response);
      return response.user; 
    } catch (error) {
      console.error("Error en login:", error);
    }
  };

  const logout = async () => {
    try {
      const response = await signOut(auth);
      console.log(response);
    } catch (error) {
      console.error("Error en logout:", error);
    }
  };

  const value = {
    user,
    register,
    login,
    logout,
    onAuthStateChanged: onAuthStateChangedHandler,
    checkAuthentication,
  };

  return <authContext.Provider value={value}>{children}</authContext.Provider>;
};
