import React, { Component } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { Home } from './pages/Home';
import { Register } from './pages/Register';
import { Login } from './pages/Login';
import { AuthProvider } from './context/AuthContext'
import { Profile } from './pages/Profile';
import { Favoritos } from './pages/Favoritos';
import SplashScreen from './components/SplashScreen';
import DetallePelicula from './pages/DetallePelicula';

export class App extends Component {
  state = {
    loading: true,
  };

  componentDidMount() {
    setTimeout(() => {
      this.setState({ loading: false });
    }, 2000);
  }

  render() {
    const { loading } = this.state;
    return (
      <AuthProvider>
        <BrowserRouter>
          <Routes>
          {loading ? (
              <Route path="*" element={<SplashScreen />} />
            ) : (
            <>
            <Route exact path="/" element={<Home />} />
            <Route exact path="/register" element={<Register />} />
            <Route exact path="/login" element={<Login />} />
            <Route exact path="/profile" element={<Profile />} />
            <Route exact path="/favoritos" element={<Favoritos />}/>
            <Route exact path="/detalle/:id" element={<DetallePelicula />}/>
            </>
              )}
          </Routes>
        </BrowserRouter>
      </AuthProvider>
    );
  }
}

export default App;

