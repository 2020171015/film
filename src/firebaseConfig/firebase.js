import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
import {getFirestore} from "firebase/firestore";
import { getAuth } from "firebase/auth";
// eslint-disable-next-line no-unused-vars
import {getMessaging, getToken} from "firebase/messaging";


const firebaseConfig = {
  apiKey: "AIzaSyAEzUz_AR8rYAVmQaTSeHWRUxpNffyuvCw",
  authDomain: "film-app-f61ea.firebaseapp.com",
  databaseURL: "https://film-app-f61ea-default-rtdb.firebaseio.com",
  projectId: "film-app-f61ea",
  storageBucket: "film-app-f61ea.appspot.com",
  messagingSenderId: "1035227569978",
  appId: "1:1035227569978:web:9df2954415d46951e27a03",
  measurementId: "G-P9F31097KT"
};

const app = initializeApp(firebaseConfig);
// eslint-disable-next-line no-unused-vars
const analytics = getAnalytics(app);
export const db = getFirestore(app);
export const auth = getAuth(app);
export const messaging = getMessaging(app);