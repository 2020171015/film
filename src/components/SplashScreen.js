import React from 'react';
import '../styles/SplashScreen.css'

import splash from '../assets/splash.gif';

const SplashScreen = () => {
  return (
    <div className="splash-screen">
      <h1 className="title-splash">FILM</h1>
      <p>Cargando...</p>
      <img className="logo-splash" src={splash} alt="Logo" />
    </div>
  );
};

export default SplashScreen;
