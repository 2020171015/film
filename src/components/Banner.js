import React from 'react';
import ReactPlayer from "react-player";
import Joker from '../assets/joker.mp4';
import '../styles/Banner.css'

export const Banner = () => {
  return (
    <div className="banner">
        <ReactPlayer className="video"
          width='100%'
          height='auto'
          url={Joker}
          playing
          controls
        />
    </div>
  )
}
