import React from 'react';
import { useNavigate } from 'react-router-dom';
import { useAuth } from '../context/AuthContext';

export const Logout = () => {
    const navigate = useNavigate();
    const auth = useAuth();

    const handleLogout = () => {
        auth.logout();
        navigate('/');
    };

  return (
    <div className="navbar-logout-box">
        <button onClick={handleLogout}>Cerrar Sesión</button>
    </div>
  )
}
