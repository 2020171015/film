import React, { useState, useEffect, useCallback } from 'react';
import axios from 'axios';
import { useAuth } from '../context/AuthContext';
import like from '../assets/like.png';
import { Banner } from './Banner';
import '../styles/Peliculas.css';
import { Link } from 'react-router-dom'; 
import { getFirestore, collection, setDoc, doc, getDoc } from 'firebase/firestore';

const Peliculas = () => {
  const [peliculas, setPeliculas] = useState([]);
  const [searchTerm, setSearchTerm] = useState('');
  const auth = useAuth();
  const [message, setMessage] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  // eslint-disable-next-line no-unused-vars
  const [messageTimeout, setMessageTimeout] = useState(null);
  const db = getFirestore();

  const getPeliculas = useCallback(async (search) => {
    try {
      const response = await axios.get(
        `https://us-central1-film-app-f61ea.cloudfunctions.net/app/api/peliculas?search=${search || ''}`
      );

      setPeliculas(response.data);
    } catch (error) {
      console.error('Error al obtener las películas:', error);
    }
  }, []);

  useEffect(() => {
    const fetchData = async () => {
      await getPeliculas();
    };

    fetchData();
  }, [getPeliculas]);

  const handleCorazonClick = async (peliculaId) => {
    setErrorMessage('');
    setMessage('');
  
    try {
      const userUID = auth.user.uid;
      const userID = auth.user.uid;
  
      const favoritosCollection = collection(db, 'favoritos');
      
      const favoritosRef = doc(favoritosCollection, userID);
      
      const favoritosSnapshot = await getDoc(favoritosRef);
  
      let favoritosArray = [];
  
      if (favoritosSnapshot.exists()) {
        favoritosArray = favoritosSnapshot.data().favoritos || [];
  
        if (favoritosArray.includes(peliculaId)) {
          setErrorMessage('La película ya está en la lista de favoritos');
          return;
        }
      }
  
      const peliculaRef = doc(db, 'peliculas', peliculaId);
      const peliculaSnapshot = await getDoc(peliculaRef);
  
      if (!peliculaSnapshot.exists()) {
        setErrorMessage('Película no encontrada');
        return;
      }
  
      favoritosArray.push(peliculaId);
  
      await setDoc(favoritosRef, { favoritos: favoritosArray, userUID });
  
      setMessage('Película agregada a favoritos');
      const timeout = setTimeout(() => {
        setMessage('');
      }, 3000);
  
      setMessageTimeout(timeout);
    } catch (error) {
      console.error('Error al agregar la película a favoritos:', error);
    }
  };
  
  
  
  
  return (
    <>
      <Banner />
      <div className="search-bar">
        <input
          type="text"
          id='buscar'
          placeholder="Buscar películas..."
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
          autoComplete="off" 
        />
      </div>
      <section>
        {message && <div className='alert alert-success'>{message}</div>}
        {errorMessage && <div className='alert alert-danger'>{errorMessage}</div>}
        <div className="movie-list">
          {peliculas
            .filter((pelicula) => pelicula.titulo.toLowerCase().includes(searchTerm.toLowerCase()))
            .map((pelicula) => (
                <div className="movie-card" key={pelicula.id}>
                  <div className="movie-title">{pelicula.titulo}</div>
                  <Link key={pelicula.id} to={`/detalle/${pelicula.id}`} state={{ peliculaId: pelicula.id }}>
                    <div className="movie-image">
                      <img src={pelicula.imagenURL} alt={`Póster de ${pelicula.titulo}`}/>
                    </div>
                  </Link>    
                  <div className="descripcion-container">
                    <div className="movie-description">{pelicula.descripcion}</div>
                    {auth.user ? (
                      <div className="favorito">
                        <button onClick={() => handleCorazonClick(pelicula.id)}>
                          <img src={like} alt="Corazón" className="corazon" />
                        </button>
                      </div>
                    ) : null}
                  </div>
                </div>
             
            ))}
        </div>
      </section>
    </>
  );
};

export default Peliculas;
