import React, { useEffect, useState } from 'react'
import logo from '../assets/logo.png'
import { Link } from 'react-router-dom';
import { useAuth } from '../context/AuthContext';
import { Logout } from './Logout';
import '../styles/Navbar.css';

export const Navbar = () => {
  const [showLogoutBox, setShowLogoutBox] = useState(false);
  const auth = useAuth();

  useEffect(() => {
    function changeCss() {
      const navElement = document.querySelector('header');

      if (window.scrollY > 50) {
        navElement.style.marginTop = 0;
        navElement.style.position = 'fixed';
        navElement.style.backgroundColor = '#1A1A1B';
        navElement.style.borderRadius = 0;
        navElement.style.width = '100%';
        navElement.style.margin = 'auto';
        navElement.style.padding = 0;
        navElement.style.top = '0';
        navElement.style.boxShadow = '0px 20px 45px rgb(0, 0, 0) inset';
        navElement.style.paddingLeft = "20px";
        navElement.style.paddingRight = "20px";
      } else {
        navElement.style.marginTop = '';
        navElement.style.position = 'absolute';
        navElement.style.backgroundColor = '';
        navElement.style.borderRadius = '';
        navElement.style.width = '';
        navElement.style.boxShadow = '0px 45px 35px rgb(0, 0, 0) inset';
        navElement.style.margin = '';
        navElement.style.padding = '';
        navElement.style.top = '';
      }
    }

    window.addEventListener('scroll', changeCss, false);

    return () => {
      window.removeEventListener('scroll', changeCss, false);
    };
  }, []);

  const handleProfileClick = () => {
    setShowLogoutBox(!showLogoutBox);
  };

  return (
    <header>
        <div className='logo'>
        <Link to='/'>
          <img src={logo} alt="Logo" className="logo" />
        </Link>
        </div>
        <nav>
        {!auth.user && <Link to="/login">Iniciar Sesión</Link>}
        {!auth.user && <Link to="/register">Registrarme</Link>}
          {auth.user ? (
          <>
            <Link to="/favoritos">Favoritos</Link>
            <Link to="/profile" onClick={handleProfileClick}>Mi Perfil</Link>
            <div className='user'>{auth.user.email}</div>
            {showLogoutBox && (
              <Logout />
            )}
          </>
        ) : null}
        </nav>
    </header>
  )
}
