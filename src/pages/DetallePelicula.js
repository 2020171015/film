import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { Navbar } from '../components/Navbar';
import '../styles/DetallePelicula.css'; 

const DetallePelicula = () => {
  const { id } = useParams();
  const [peliculaData, setPeliculaData] = useState(null);

  useEffect(() => {
    const fetchPeliculaData = async () => {
      try {
        const response = await fetch(`https://us-central1-film-app-f61ea.cloudfunctions.net/app/api/peliculas/${id}`);
        const data = await response.json();
        setPeliculaData(data);
      } catch (error) {
        console.error('Error al obtener los detalles de la película:', error);
      }
    };

    fetchPeliculaData();
  }, [id]);

  return (
    <>
      <Navbar />
      <div className='detalle-pelicula-container'>
        {peliculaData ? (
          <div className='detalle-card'>
            <h2 className='detalle-title'>{peliculaData.titulo}</h2>
            <div className='detalle-content'>
              <div className='detalle-image'>
                <img src={peliculaData.imagenURL} alt={`Póster de ${peliculaData.titulo}`} />
              </div>
              <div className='detalle-info'>
                <div>
                </div>
                <div>
                  <h3>Descripción</h3>
                  <p>{peliculaData.descripcion}</p>
                </div>
                <div>
                  <h3>Comentarios</h3>
                  <ul>
                    {peliculaData.comentarios.map((comentario) => (
                      <li key={comentario.id}>{comentario.texto}</li>
                    ))}
                  </ul>
                </div>
              </div>
            </div>
          </div>
        ) : (
          <p>Cargando detalles de la película...</p>
        )}
      </div>
    </>
  );
};

export default DetallePelicula;
