import React, { useEffect, useState, useCallback } from 'react';
import { Navbar } from '../components/Navbar';
import axios from 'axios';
import { useAuth } from '../context/AuthContext';
import trash from '../assets/trash.png';
import { NotFavorites } from '../components/NotFavorites';
import { Modal, Button } from 'react-bootstrap';
import '../styles/Favoritos.css'

export const Favoritos = () => {
  const [peliculas, setPeliculas] = useState([]);
  const auth = useAuth();
  const [loading, setLoading] = useState(true);
  const [authenticationVerified, setAuthenticationVerified] = useState(false);
  const [showAlert, setShowAlert] = useState(false);
  const [peliculaIdToDelete, setPeliculaIdToDelete] = useState('');
  const [nuevoComentario, setNuevoComentario] = useState('');
  const [showReviewModal, setShowReviewModal] = useState(false);
  const [selectedPeliculaId, setSelectedPeliculaId] = useState(null);

  const fetchData = useCallback(async () => {
    try {
      if (auth.user && auth.user.uid) {
        const uid = auth.user.uid;
        const response = await axios.get(`https://us-central1-film-app-f61ea.cloudfunctions.net/app/api/favoritos/${uid}`);
        //console.log('Respuesta:', response.data);
        setPeliculas(response.data);
      } else {
        console.warn('El usuario no tiene un ID.');
      }
    } catch (error) {
      console.error('Error al obtener las películas:', error);
    } finally {
      setLoading(false);
    }
  }, [auth]);

  const handleEliminarClick = async (peliculaID) => {
    setShowAlert(true);
    setPeliculaIdToDelete(peliculaID);
  };

  const confirmDelete = async () => {
    try {
      const uid = auth.user.uid;
      await axios.delete('https://us-central1-film-app-f61ea.cloudfunctions.net/app/api/favoritos', {
        data: { uid,  peliculaID: peliculaIdToDelete },
      });
      fetchData();
    } catch (error) {
      console.error('Error al eliminar la película de favoritos:', error);
    }finally {
      setShowAlert(false); 
      setPeliculaIdToDelete(''); 
    }
  };


  useEffect(() => {
    const checkAuthentication = async () => {
      try {
        await auth.checkAuthentication();
        setAuthenticationVerified(true);
      } catch (error) {
        console.error('Error al verificar la autenticación:', error);
      } finally {
        setLoading(false);
      }
    };

    checkAuthentication();
  }, [auth]);

  useEffect(() => {
    if (authenticationVerified && auth.user && auth.user.uid) {
      fetchData();
    }
  }, [authenticationVerified, auth.user, fetchData]);

  const handleEscribirResenaClick = (peliculaID) => {
    setShowReviewModal(true);
    setSelectedPeliculaId(peliculaID);
  };

  const handleCloseReviewModal = () => {
    setShowReviewModal(false);
    setSelectedPeliculaId(null);
  };  

  const enviarComentario = async (peliculaID, e) => {
    e.preventDefault();
    try {
      const uid = auth.user.uid;
  
      const response = await fetch('https://us-central1-film-app-f61ea.cloudfunctions.net/app/api/comentarios', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          peliculaID,
          uid,
          comentario: nuevoComentario,
        }),
      });
  
      if (!response.ok) {
        throw new Error(`Error al enviar el comentario. Código de estado: ${response.status}`);
      }
  
      setNuevoComentario('');
      setShowReviewModal(false);
      setSelectedPeliculaId(null);
      await fetchData();
    } catch (error) {
      console.error('Error al enviar el comentario:', error);
    }
  };
  

  return (
    <>
      <Navbar />
      <div className="container-fav">
        <br />
        <section>
          {loading ? (
            <p>Cargando...</p>
          ) : peliculas.length > 0 ? (
            <div className="movie-list-fav">
              {peliculas.map((pelicula) => (
                <div className="movie-card-fav" key={pelicula.id}>
                  <div className="descripcion-container-fav">
                    <div className="movie-image-fav">
                      <div className="movie-title-fav">{pelicula.titulo}</div>
                      <img
                        src={pelicula.imagenURL}
                        alt={`Póster de ${pelicula.titulo}`}
                      />
                    </div>
                    <div className="input-trash-container">
                      <div>
                        <div className="fav">
                          <button onClick={() => handleEscribirResenaClick(pelicula.id)}>
                            Tu reseña
                          </button>
                        </div>
                        {pelicula.comentarios ? (
                        <p>{pelicula.comentarios.texto}</p>
                        ) : (
                          <p>Escribe una reseña...</p>
                        )}
                      </div>
                      <div className="trash">
                        <button onClick={() => handleEliminarClick(pelicula.id)}>
                          <img src={trash} alt="Trash" />
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              ))}
            </div>
          ) : (
            <NotFavorites />
          )}
        </section>
        <Modal show={showAlert} onHide={() => setShowAlert(false)}>
          <Modal.Header closeButton>
            <Modal.Title>Confirmar eliminación</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            ¿Estás seguro de que quieres eliminar esta película de tus favoritos?
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={() => setShowAlert(false)}>
              Cancelar
            </Button>
            <Button variant="danger" onClick={confirmDelete}>
              Eliminar
            </Button>
          </Modal.Footer>
        </Modal>
        <Modal show={showReviewModal} onHide={handleCloseReviewModal}>
        <Modal.Header closeButton>
          <Modal.Title>Escribir reseña</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <input
            type="text"
            placeholder="Escribe tu reseña aquí"
            value={nuevoComentario}
            onChange={(e) => setNuevoComentario(e.target.value)}
          />
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => {handleCloseReviewModal(); setSelectedPeliculaId(null)}}>
            Cancelar
          </Button>
          <Button variant="primary" onClick={(e) => enviarComentario(selectedPeliculaId, e)}>
            Enviar
          </Button>
        </Modal.Footer>
      </Modal>
      </div>
    </>
  );
};
