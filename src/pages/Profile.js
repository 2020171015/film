import React, { useEffect, useState } from 'react';
import { useAuth } from '../context/AuthContext';
import axios from 'axios';
import { Navbar } from '../components/Navbar';
import {getToken, onMessage} from 'firebase/messaging';
import { messaging } from '../firebaseConfig/firebase';
import {ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import '../styles/Form.css'

export const Profile = () => {
  const auth = useAuth();
  const [newName, setNewName] = useState('');
  const [newPassword, setNewPassword] = useState('');
  const [userInfo, setUserInfo] = useState(null);
  const [loading, setLoading] = useState(true);
  const [message, setMessage] = useState('');
  const [errorMessage, setErrorMessage] = useState('');
  // eslint-disable-next-line no-unused-vars
  const [messageTimeout, setMessageTimeout] = useState(null);

  const getTokenNotification = async () => {
    const token = await getToken(messaging, {
        vapidKey: 'BHOo0-gm0kyIe4ZiLztzzZYI5m2bZAVs4J1C8Uxdjw9D1Ufe9eAlWJdDPnSO4Zs5nLo27kX1CCUW_ROMs5Na4Ks'
    }).catch((err) => console.log('No se pudo obtener el token', err))
    if (token) {
        console.log('Token: ', token)
    }if (!token) {
        console.log("No hay token disponible")
    }
  }

  const notificarme = () => {
    if (!window.Notification) {
      console.log('Este navegador no soporta notificaciones');
      return;
    }
  
    if (Notification.permission === 'granted') {
      getTokenNotification();
    } else if (Notification.permission !== 'denied' || Notification.permission === 'default') {
      Notification.requestPermission().then((permission) => {
        console.log(permission);
        if (permission === 'granted') {
          getTokenNotification();
        }
      });
    }
  };
  
  // Llamada a la función corregida
  notificarme();

  useEffect(() => {
    getTokenNotification()
    onMessage(messaging, message => {
        console.log('onMessage: ', message)
        toast(message.notification.title)
    })
  }, [])

  useEffect(() => {
    const fetchUserInfo = async () => {
      try {
        if (auth.user && auth.user.uid) {
          const uid = auth.user.uid;
          const response = await axios.get(`https://us-central1-film-app-f61ea.cloudfunctions.net/app/api/users/${uid}`);
  
          if (response.data) {
            setUserInfo(response.data);
            setNewName(response.data.name || '');
          } else {
            console.error('No se encontró la información del usuario en la base de datos.');
          }
        } else {
          console.error('El usuario no autenticado.');
        }
      } catch (error) {
        console.error('Error al obtener la información del usuario:', error);
      } finally {
        setLoading(false);
      }
    };
  
    if (auth.user) {
      fetchUserInfo();
    }
  }, [auth.user]);
  

  useEffect(() => {
    const checkAuthentication = async () => {
      try {
        await auth.checkAuthentication();
      } catch (error) {
        console.error('Error al verificar la autenticación:', error);
      }
    };

    checkAuthentication();
  }, [auth]);

  const handleUpdateProfile = async (e) => {
    e.preventDefault();
    setErrorMessage('');
    setMessage('');
  
    try {
      if (userInfo) {
        const userId = userInfo.id;
  
        await axios.put(`https://us-central1-film-app-f61ea.cloudfunctions.net/app/api/users/${userId}`, {
          name: newName,
          password: newPassword,
        });
  
        //console.log('Perfil actualizado exitosamente');
        setMessage('Perfil actualizado exitosamente');
        const timeout = setTimeout(() => {
          setMessage('');
        }, 3000);
  
        setMessageTimeout(timeout);
      } else {
        console.error('No se puede actualizar el perfil porque no se ha obtenido la información del usuario.');
      }
    } catch (error) {
      console.error('Error al actualizar el perfil:', error);
      setErrorMessage('Error al editar, verifica que la contraseña tenga minimo 6 digitos');
      const timeout = setTimeout(() => {
        setErrorMessage('');
      }, 3000);

      setMessageTimeout(timeout);
    }
  };
  

  if (loading) {
    return <p>Cargando...</p>;
  }

  return (
    <div>
      <Navbar />
      <ToastContainer/>
      <div className='container'>
        <br />
        <h2>MI PERFIL</h2>
        <hr />
        {message && <div className='alert alert-success'>{message}</div>}
        {errorMessage && <div className='alert alert-danger'>{errorMessage}</div>}
        {auth.user ? (
          <form autoComplete='off' className='form-group'>
            <div className="input-container">
              <label htmlFor='newEmail'>Correo Electrónico</label>
              <div className="styled-input">{auth.user.email}</div>
            </div>
            <br/>
            <div className="input-container">
              <label htmlFor='newName' id='name'>Nuevo Nombre</label>
              <input
                type='text'
                className='form-control'
                id='newName'
                onChange={(e) => setNewName(e.target.value)}
                value={newName}
                autoComplete='new-name'
              />
            </div>
            <br />
            <div className="input-container">
            <label htmlFor='newPassword' id='password'>Nueva Contraseña</label>
            <input
              type='password'
              className='form-control'
              id='newPassword'
              onChange={(e) => setNewPassword(e.target.value)}
              value={newPassword}
              autoComplete='new-password'
            />
            </div>
            <br />
            <div className="button-container">
              <button type='submit' className='btn btn-primary btn-md mybtn' onClick={handleUpdateProfile}>
                Actualizar Perfil
              </button>
            </div>
          </form>
        ) : (
          <p>No estás autenticado.</p>
        )}
        <br />
      </div>
    </div>
  );
};
