import React from 'react';
import { Navbar } from '../components/Navbar';
import Peliculas from '../components/Peliculas';

export const Home = () => {
  return (
    <div>
        <Navbar/>
        <Peliculas/>
    </div>
  )
}
