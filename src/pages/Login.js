import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { useAuth } from '../context/AuthContext';
import { Navbar } from '../components/Navbar';
import { onAuthStateChanged } from 'firebase/auth';
import '../styles/Form.css'

export const Login = () => {
  const auth = useAuth()
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState(null);
  const navigate = useNavigate(); 

  const handleLogin = async (e) => {
    e.preventDefault();
    //console.log("Intentando iniciar sesión con:", email, password);
    if (password.length < 6) {
      console.log('La contraseña debe tener al menos 6 caracteres.');
      setError("Contraseña debe tener 6 caracteres");
      return;
    }

    try {
      await auth.login(email, password);

      const unsubscribe = onAuthStateChanged(auth, (currentUser) => {
        if (currentUser) {
          setError(null);
          navigate('/profile');
          unsubscribe();
        } else {
          setError('Correo o contraseña incorrectos. Verifica tus credenciales.');
        }
      });
    } catch (error) {
      console.error('Error al iniciar sesión:', error);
      setError('Correo o contraseña incorrectos. Verifica tus credenciales.');
    }
  };
  
  return (
    <div>
    <Navbar/>
        <div className='container'>
            <br />
            <h2>INICIAR SESIÓN</h2>
            <hr />
            {error && <div className="alert alert-danger">{error}</div>}
            <form autoComplete='off' className='form-group' onSubmit={handleLogin}>
              <div className="input-container">
                <label htmlFor='email'>Correo</label>
                <input
                  type='email'
                  className='form-control'
                  id='email' 
                  required
                  onChange={(e) => setEmail(e.target.value)}
                  value={email}
                  autoComplete="off" 
                />
              </div>
                <br/>
                <div className="input-container">
                <label htmlFor='password'>Contraseña</label>
                <input
                type='password'
                className='form-control'
                id='password' 
                required
                onChange={(e) => setPassword(e.target.value)}
                value={password}
                autoComplete="off" 
                />
                </div>
                <br />
                <div className="button-container">
                  <button type='submit' className='btn btn-success btn-md mybtn'>
                  Iniciar Sesión
                  </button>
                </div> 
            </form>
            <br />
            <span>
                ¿Aún no tienes una cuenta? Registrate
                <Link to='/register'>Aquí</Link>
            </span>
        </div>
    </div>
  );
};
