import { render } from '@testing-library/react';
import App from './App';

jest.mock('firebase/auth', () => ({
  getAuth: jest.fn(),
}));

jest.mock('@firebase/analytics', () => ({
  getAnalytics: jest.fn(),
}));

jest.mock('firebase/messaging', () => ({
  getMessaging: jest.fn(),
}));

test('renders learn react link', () => {
  render(<App />);
});
