# FILM APP

Una galería de películas que te permite buscar por título, agregar películas a tus favoritos y ver detalles como la imagen y descripción de la película.

## Tabla de Contenidos

1. [Instalación](#instalación)
2. [Uso](#uso)
3. [Funcionalidades](#funcionalidades)
4. [Contribuciones](#contribuciones)
5. [Licencia](#licencia)

## Instalación

Asegúrate de tener los siguientes requisitos antes de comenzar la instalación.

- Node.js y npm instalados

Sigue estos pasos para instalar el proyecto:

# Clona el repositorio desde GitLab
git clone https://gitlab.com/2020171015/film.git

# Ingresa al directorio del proyecto
cd film

# Instala las dependencias del servidor
npm install

# Ingresa al directorio de funciones de Firebase
cd server/functions

# Instala Firebase CLI globalmente
npm install -g firebase-tools

# Inicia sesión en Firebase
firebase login

# Regresa al directorio del servidor
cd ..

# Inicia el servidor de Firebase
firebase serve

## Uso
Una vez que hayas iniciado sesión en Firebase y configurado el servidor, sigue estos pasos para ejecutar la aplicación:

# Desde el directorio del proyecto, inicia la aplicación React
npm start

Esto iniciará la aplicación React en http://localhost:3000. Puedes acceder a la aplicación en tu navegador.

## Funcionalidades
Crear Cuenta: Permite a los usuarios crear nuevas cuentas, requieres el nombre, correo y contraseña (la contraseña tiene que tener un minimo de 6 caracteres).

Iniciar Sesión: Permite a los usuarios iniciar sesión en sus cuentas colocando el correo y contraseña que registraron con anterioridad.

Mi Perfil: Accede a tu perfil para ver y actualizar tus datos.

Actualizar Datos del Perfil: Modifica la información de tu perfil.

Buscar Películas: Utiliza la barra de búsqueda para encontrar películas por título.

Ver Detalles: Puedes ver el titulo de la película con su imagen y descripción.

Agregar a Favoritos: Marca tus películas favoritas y consúltalas más tarde en el apartado de favoritos.

Eliminar favoritos: Tienes la opcion de eliminar las peliculas de tus favoritos a voluntad.

## Contribuciones
Agradecemos las contribuciones. Para contribuir:

Haz un fork del proyecto.

Crea una rama para tu contribución (git checkout -b feature/nueva-caracteristica).

Realiza tus cambios y haz commit (git commit -am 'Añade nueva característica').

Haz push a la rama (git push origin feature/nueva-caracteristica).

Abre un pull request.

## Licencia
Este proyecto está licenciado bajo la Licencia MIT.