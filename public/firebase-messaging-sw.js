importScripts('https://www.gstatic.com/firebasejs/10.6.0/firebase-app-compat.js');
importScripts('https://www.gstatic.com/firebasejs/10.6.0/firebase-messaging-compat.js');

const firebaseConfig = {
  apiKey: "AIzaSyAEzUz_AR8rYAVmQaTSeHWRUxpNffyuvCw",
  authDomain: "film-app-f61ea.firebaseapp.com",
  databaseURL: "https://film-app-f61ea-default-rtdb.firebaseio.com",
  projectId: "film-app-f61ea",
  storageBucket: "film-app-f61ea.appspot.com",
  messagingSenderId: "1035227569978",
  appId: "1:1035227569978:web:9df2954415d46951e27a03",
  measurementId: "G-P9F31097KT"
};

const app = firebase.initializeApp(firebaseConfig);
const messaging = firebase.messaging(app);

messaging.onBackgroundMessage(payload => {
    console.log('Recibiendo mensaje en segundo plano')
    const tituloNotificacion = payload.notification.title;
    const options = {
        body: payload.notification.body,
        icon: '../img/trash.png'
    }
    self.registration.showNotification(tituloNotificacion, options)
})