importScripts('https://cdn.jsdelivr.net/npm/pouchdb@8.0.1/dist/pouchdb.min.js');
importScripts('js/sw-utils.js');
importScripts('js/sw-db.js');
importScripts('firebase-messaging-sw.js');

const CACHE_STATIC = 'static-v1';
const CACHE_DYNAMIC = 'dynamic-v1';
const CACHE_INMUTABLE = 'inmutable-v1';

const APP_SHELL =[
    '/',
    '/login',
    '/register',
    '/profile',
    '/favoritos',
    '/index.html',
    '/js/app.js',
    '/js/sw-utils.js',
    '/js/sw-db.js',
    '/sw.js',
    '/static/js/bundle.js', 
    '/manifest.json',
]

const APP_SHELL_INMUTABLE = [
    'https://www.gstatic.com/firebasejs/10.6.0/firebase-app-compat.js',
    'https://www.gstatic.com/firebasejs/10.6.0/firebase-messaging-compat.js',
    'https://cdn.jsdelivr.net/npm/pouchdb@8.0.1/dist/pouchdb.min.js',
    'https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.2/css/all.min.css',
    'https://fonts.googleapis.com/css2?family=Pacifico&display=swap'
]

//PROCESO DE INSTALACIÓN
self.addEventListener('install', function(event) {
  const cacheStatic = caches.open(CACHE_STATIC).then(cache => cache.addAll(APP_SHELL))
  const cacheInmutable = caches.open(CACHE_INMUTABLE).then(cache => cache.addAll(APP_SHELL_INMUTABLE))

  event.waitUntil(Promise.all([cacheStatic, cacheInmutable]));
})


//PROCESO DE ACTIVACION
self.addEventListener('activate', function(event){
  const respuesta =caches.keys()
    .then(keys=>{
      keys.forEach(key=>{
        if(key!== CACHE_STATIC && key.includes('static')){
            return caches.delete(key)
        }
      })
    })
    event.waitUntil(respuesta)
})


//ESTRATEGIA DE CACHE
self.addEventListener('fetch', event => {
    
  let respuesta

  if(event.request.url.includes('/app/api')){
    respuesta = manejoApi(CACHE_DYNAMIC, event.request)
  } else{
    respuesta = caches.match(event.request)
        .then(res => {
            if (res) {
              actualizaCacheStatico(CACHE_STATIC, event.request, APP_SHELL_INMUTABLE)
                return res
            } else {
                return fetch(event.request)
                    .then(newRes => {
                        return actualizaCacheDinamico(CACHE_DYNAMIC, event.request, newRes)
                    });
            }
        });
  }

  event.respondWith(respuesta);
})

// Tareas asíncronas
self.addEventListener('sync', event=>{
    console.log('SW: Sync')
    if(event.tag === 'nuevo-post'){
        // Postear a BD cuando hay conexión
        const respuesta = postearMensaje()
        event.waitUntil(respuesta)
    }
})
