const functions = require("firebase-functions");
const admin = require("firebase-admin");
const express = require("express");
const cors = require("cors");

const app = express();
app.use(cors());

admin.initializeApp({
  credential: admin.credential.cert("./permissions.json"),
  databaseURL: "https://film-app-f61ea-default-rtdb.firebaseio.com",
});

const db = admin.firestore();

/** Mensaje Hola mundo */
app.get("/hello-world", (req, res) => {
  return res.status(200).json({message: "hola mundo"});
});

/** Función para registrar usuario */
app.post("/api/users", async (req, res) => {
  try {
    await db
        .collection("users")
        .doc()
        .create({
          name: req.body.name,
        });
    return res.status(200).json();
  } catch (error) {
    return res.status(500).send(error);
  }
});

/** Traer todas las peliculas */
app.get("/api/peliculas", async (req, res) => {
  try {
    const searchTerm = req.query.search || "";

    const query = db.collection("peliculas");

    const querySnapShot = searchTerm ?
      await query.where("titulo", ">=", searchTerm).get() :
      await query.get();

    const docs = querySnapShot.docs;
    const response = docs.map((doc) => ({
      id: doc.id,
      titulo: doc.data().titulo,
      imagenURL: doc.data().imagenURL,
      descripcion: doc.data().descripcion,
    }));

    return res.status(200).json(response);
  } catch (error) {
    console.error("Error al obtener las películas:", error);
    return res.status(500).json();
  }
});

/** Mostrar usuarios */
app.get("/api/users", async (req, res) => {
  try {
    const usersRef = db.collection("users");
    const usersSnapshot = await usersRef.get();

    const users = [];
    usersSnapshot.forEach((doc) => {
      users.push({
        id: doc.id,
        name: doc.data().name,
      });
    });

    return res.status(200).json(users);
  } catch (error) {
    return res.status(500).json({error: error.message});
  }
});

/** Mostrar usuarios por uid */
app.get("/api/users/:uid", async (req, res) => {
  try {
    console.log("Received GET request for:", req.url);

    const uid = req.params.uid;
    console.log("UID from URL:", uid);

    const usersRef = db.collection("users");
    const userSnapshot = await usersRef.where("uid", "==", uid).get();

    if (userSnapshot.empty) {
      console.log("User not found");
      return res.status(404).json({error: "Usuario no encontrado"});
    }

    const userDoc = userSnapshot.docs[0];
    const userData = {
      id: userDoc.id,
      name: userDoc.data().name,
      email: userDoc.data().email,
    };

    console.log("User data:", userData);

    return res.status(200).json(userData);
  } catch (error) {
    console.error("Error:", error);
    return res.status(500).json({error: "Error al obtener el usuario"});
  }
});

/** Editar usuarios por id */
app.put("/api/users/:userId", async (req, res) => {
  try {
    const userId = req.params.userId;
    const updatedData = req.body;

    const userRef = db.collection("users").doc(userId);

    const userSnapshot = await userRef.get();
    const userFirestoreData = userSnapshot.data();
    const authUid = userFirestoreData.uid;

    await userRef.update({
      name: updatedData.name,
    });

    await admin.auth().updateUser(authUid, {
      password: updatedData.password || undefined,
    });

    return res.status(200).json({message: "Usuario actualizado exitosamente"});
  } catch (error) {
    console.error("Error:", error);
    return res.status(500).json({error: "Error al actualizar el usuario"});
  }
});

/** Agregar a favoritos */
app.post("/api/favoritos", async (req, res) => {
  try {
    const userUID = req.body.uid;
    const peliculaID = req.body.peliculaID;

    const userRef = db.collection("users").where("uid", "==", userUID);
    const userSnapshot = await userRef.get();

    if (userSnapshot.empty) {
      return res.status(404).json({error: "Usuario no encontrado"});
    }

    const userID = userSnapshot.docs[0].id;

    const favoritosRef = db.collection("favoritos").doc(userID);
    const favoritosSnapshot = await favoritosRef.get();

    let favoritosArray = [];

    if (favoritosSnapshot.exists) {
      favoritosArray = favoritosSnapshot.data().favoritos || [];


      if (favoritosArray.includes(peliculaID)) {
        return res.status(400).json({error: "ya esta agregada a favoritos"});
      }
    }

    const peliculaRef = await db.collection("peliculas").doc(peliculaID).get();

    if (!peliculaRef.exists) {
      return res.status(404).json({error: "Película no encontrada"});
    }

    favoritosArray.push(peliculaID);

    await favoritosRef.set({favoritos: favoritosArray, userID, userUID});

    return res.status(200).json({message: "Película agregada a favoritos"});
  } catch (error) {
    console.error("Error:", error);
    return res.status(500).json({error: error.message});
  }
});

/** Mostrar favoritos por usuario */
app.get("/api/favoritos/:uid", async (req, res) => {
  try {
    const uid = req.params.uid;

    const userRef = db.collection("users").where("uid", "==", uid);
    const userSnapshot = await userRef.get();

    if (userSnapshot.empty) {
      return res.status(404).json({error: "Usuario no encontrado"});
    }

    const userDoc = userSnapshot.docs[0];
    const userUID = userDoc.data().uid;

    const favoritosRef = db.collection("favoritos").doc(userUID);
    const favoritosSnapshot = await favoritosRef.get();

    if (!favoritosSnapshot.exists) {
      return res.status(200).json({message: "El usuario no tiene favoritos"});
    }

    const favoritosArray = favoritosSnapshot.data().favoritos || [];

    const peliculasFavoritas = await Promise.all(
        favoritosArray.map(async (peliculaID) => {
          const peliculaRef =
          await db.collection("peliculas").doc(peliculaID).get();

          if (peliculaRef.exists) {
            const comentarioRef = db.collection("comentarios")
                .where("peliculaID", "==", peliculaID)
                .where("userUID", "==", userUID);

            const comentarioSnapshot = await comentarioRef.get();
            let comentario = null;
            comentarioSnapshot.forEach((comentarioDoc) => {
              const comentarioData = comentarioDoc.data();
              comentario = {
                id: comentarioDoc.id,
                texto: comentarioData.comentario,
                userUID: comentarioData.userUID,
              };
            });

            return {
              id: peliculaRef.id,
              titulo: peliculaRef.data().titulo,
              imagenURL: peliculaRef.data().imagenURL,
              descripcion: peliculaRef.data().descripcion,
              comentarios: comentario,
            };
          } else {
            return null;
          }
        }),
    );
    // eslint-disable-next-line max-len
    return res.status(200).json(peliculasFavoritas.filter((pelicula) => pelicula !== null));
  } catch (error) {
    console.error("Error:", error);
    return res.status(500).json({error: error.message});
  }
});

/** Eliminar favorito */
app.delete("/api/favoritos", async (req, res) => {
  try {
    const userUID = req.body.uid;
    const peliculaID = req.body.peliculaID;

    const userRef = db.collection("users").where("uid", "==", userUID);
    const userSnapshot = await userRef.get();

    if (userSnapshot.empty) {
      return res.status(404).json({error: "Usuario no encontrado"});
    }

    const favoritosRef = db.collection("favoritos").doc(userUID);
    const favoritosSnapshot = await favoritosRef.get();

    let favoritosArray = [];

    if (favoritosSnapshot.exists) {
      favoritosArray = favoritosSnapshot.data().favoritos || [];

      if (favoritosArray.includes(peliculaID)) {
        favoritosArray = favoritosArray.filter((id) => id !== peliculaID);

        await favoritosRef.set({favoritos: favoritosArray, userUID});

        return res.status(200).json({message: "Película eliminada"});
      } else {
        return res.status(400).json({error: "Pelicula no agregada a favorito"});
      }
    } else {
      return res.status(400).json({error: "No tiene películas favoritos"});
    }
  } catch (error) {
    console.error("Error:", error);
    return res.status(500).json({error: error.message});
  }
});

/** Escribir comnetarios */
app.post("/api/comentarios", async (req, res) => {
  try {
    const peliculaID = req.body.peliculaID;
    const userUID = req.body.uid;
    const comentarioText = req.body.comentario;

    const peliculaRef = db.collection("peliculas").doc(peliculaID);
    const peliculaDoc = await peliculaRef.get();

    if (!peliculaDoc.exists) {
      return res.status(404).json({error: "Película no encontrada"});
    }

    const userRef = db.collection("users").where("uid", "==", userUID);
    const userSnapshot = await userRef.get();

    if (userSnapshot.empty) {
      return res.status(404).json({error: "Usuario no encontrado"});
    }

    const userDoc = userSnapshot.docs[0];
    const userUID2 = userDoc.data().uid;

    const comentarioRef = db.collection("comentarios")
        .where("peliculaID", "==", peliculaID)
        .where("userUID", "==", userUID2);

    const comentarioSnapshot = await comentarioRef.get();

    if (comentarioSnapshot.empty) {
      await db.collection("comentarios").doc().create({
        peliculaID: peliculaID,
        userUID: userUID2,
        comentario: comentarioText,
      });

      return res.status(200).json({message: "Comentario agregado"});
    } else {
      const comentarioID = comentarioSnapshot.docs[0].id;
      await db.collection("comentarios").doc(comentarioID).update({
        comentario: comentarioText,
      });

      return res.status(200).json({message: "Comentario actualizado"});
    }
  } catch (error) {
    console.error("Error:", error);
    return res.status(500).json({error: error.message});
  }
});

app.get("/api/peliculas/:peliculaId", async (req, res) => {
  try {
    const peliculaId = req.params.peliculaId;

    const peliculaRef = db.collection("peliculas").doc(peliculaId);
    const peliculaDoc = await peliculaRef.get();

    if (!peliculaDoc.exists) {
      return res.status(404).json({error: "Película no encontrada"});
    }

    // eslint-disable-next-line max-len
    const comentariosRef = db.collection("comentarios").where("peliculaID", "==", peliculaId);
    const comentariosSnapshot = await comentariosRef.get();

    const comentarios = comentariosSnapshot.docs.map((comentarioDoc) => ({
      id: comentarioDoc.id,
      texto: comentarioDoc.data().comentario,
      userUID: comentarioDoc.data().userUID,
    }));

    const peliculaData = {
      id: peliculaDoc.id,
      titulo: peliculaDoc.data().titulo,
      imagenURL: peliculaDoc.data().imagenURL,
      descripcion: peliculaDoc.data().descripcion,
      comentarios,
    };

    return res.status(200).json(peliculaData);
  } catch (error) {
    console.error("Error:", error);
    return res.status(500).json({error: error.message});
  }
});

exports.app = functions.https.onRequest(app);
